Taxonomy Parser Readme
======================

This module provides helper function for parsing data from a structured array
into drupal taxonomy term objects.

The module does not have an administrative interface as such and 
is designed as to be used as a utility module for other modules
wishing to import taxonomy terms from an external source (for example a .csv, .xml)

The main function : taxonomy_parser_import_taxonomy($term_categories, $type = null);

This function accepts an associative array of parent / child term names
which are to be imported into the drupal taxonomy. It will also accept an optional
node type argument if your taxonomy terms are to only be applied to a certain node type.

The array passed to this function for importing should be in the following format :
 ['parent term name'] = array(
   [1] = 'child term name',
   [2] = 'child term name',
   ['vocab'] = vocabulary name
 );
 
This module is does not provide any functionality for parsing directly 
from .csv / .xml as yet. It assumes you have already parsed your document 
into an array with the structure above.

the taxonomy_parser_import_taxonomy(); function will also return the taxonomy object
after it has been inserted into the database for you to use freely.
This is useful if you want to apply the taxonomy terms to a node whilst you 
are importing the data for example:

$node->taxonomy = taxonomy_parser_import_taxonomy($term_categories);

You do not have to use the returned taxonomy data if you do not wish to apply the taxonomy to a 
node you can just call the function to to an import of the the taxonomy terms and discard the returned 
taxonomy object.

A simple example would be where you want to import 1 term called 'foo' into
a vocabulary called 'products'. 
The $term_categories array would be structured like so.
 
$term_categories = array(
  ['foo'] = array(
    ['vocab'] = 'Products'
  )
);

This would get added to the database and the function would
return a standard drupal taxonomy object which would
look something like the following: 

$node->taxonomy = array(
  [476] = stdClass Object(
    [tid] = 476
    [vid] = 10
    [name] = foo
    [description] = ''  
    [weight] = 0
  )
); 
